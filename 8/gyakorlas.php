<?php
require "../7/connect.php";//db csatlakozás
//ha jön urlből feldolgozható adat akkor dolgozzuk fel (törlés)
$employee_to_delete = filter_input(INPUT_GET,'del');
if($employee_to_delete > 0){
    //töröljük az adatbázisból
     mysqli_query($link,'DELETE FROM employees WHERE employeenumber = '.$employee_to_delete) or die(mysqli_error($link));
     //url takarítás, mert benne marad az employeenumber így
    header('location:'.$_SERVER['PHP_SELF']);exit();
}

$qry = "SELECT * FROM employees";
$result = mysqli_query($link,$qry) or die(myslqi_error($link));
$employees = '<a href="new_employee.php">új felvitel</a><ul>';
//kibontás ciklusban
while(null !== $row = mysqli_fetch_assoc($result)){
    //echo '<pre>'.var_export($row,true).'</pre>';//https://www.php.net/manual/en/function.var-export.php
    $employees .= '<li>'.implode(' | ',$row).' | <a href="edit_employee.php?update='.$row['employeeNumber'].'">módosít</a> | <a class="confirmation" href="?del='.$row['employeeNumber'].'">X</a></li>';
}
$employees .= '</ul>';
echo $employees;

$scripts = "<script type=\"text/javascript\">
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Tutiegészen biztos vagy benne?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
</script>";
echo $scripts;