<?php
//connect az előző óráról
require "../7/connect.php";
//itt tudunk javasolni egy employeenumbert
$result = mysqli_query($link,"SELECT MAX(employeenumber)+1 FROM employees") or die(mysqli_error($link));
$row = mysqli_fetch_row($result);
$employeenumber_helper = $row[0];

if(!empty($_POST)) {
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];

    //firstname kötelező
    $firstname = filter_input(INPUT_POST,'firstname');
    //mysqli injection védelem a mezőre
    $firstname = mysqli_real_escape_string($link,$firstname);
    if(!$firstname){
        $hiba['firstname']='<span class="error">Kötelező kitölteni!</span>';
    }
    //lastname kötelező
    $lastname = filter_input(INPUT_POST,'lastname');
    if(!$lastname){
        $hiba['lastname']='<span class="error">Kötelező kitölteni!</span>';
    }

    //email kötelező és email formátum
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
    if(!$email){
        $hiba['email']='<span class="error">Nem megfelelő formátum!</span>';
    }

    //employeenumber csak pozitiv egész lehet
    $employeenumber = filter_input(INPUT_POST,'employeenumber',FILTER_VALIDATE_INT);
    if($employeenumber < 1 ){
        $hiba['employeenumber']='<span class="error">Nem megfelelő formátum!</span>';
    }else{
        //nem foglalt-e a dbben az employeenumber, mert primary key és ezért nem ismétlődhet
        $qry = "SELECT COUNT(*) FROM employees WHERE employeenumber = $employeenumber ";
        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        //var_dump($result);//objektum property elérése
        $row = mysqli_fetch_row($result);
        if($row[0] > 0){//ha volt már ilyen az adatbázisba, akkor a num_rows 1 -> ez nem igaz, mert mindenképp lesz 1 sora a count értéke,(jelen esetben 0 vagy 1), ezért fetchelni kell ^^
            $hiba['employeenumber']='<span class="error">Foglalt azonosító!</span>';
        }

    }
    //szöveges mezők sql injection védelme - (nem árt mindenre ami nem erősebb kapott filtert mint a default)
    $extension = filter_input(INPUT_POST,'extension');
    $extension = mysqli_real_escape_string($link,$extension);
    $jobtitle = filter_input(INPUT_POST,'jobtitle');
    $jobtitle = mysqli_real_escape_string($link,$jobtitle);

    //ha üres a hiba tömb, nem volt hiba
    if(empty($hiba)){
        //egyszerű adatbázisba írás
      $qry =  "INSERT INTO 
    `employees` (`employeeNumber`, `lastName`, `firstName`, `extension`, `email`, `officeCode`, `reportsTo`, `jobTitle`) 
    VALUES ('$employeenumber', '$lastname', '$firstname', '$extension', '$email', '3', '1002', '$jobtitle');";
        //DB BEÍRÁS
        mysqli_query($link,$qry) or die(mysqli_error($link));
        //visszairányitjuk a filet a listázásra
        header('location:gyakorlas.php');exit();

    }

}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Új felhasználó felvitele</title>
    <style>
        form {
            margin: 0 auto;
            width: 500px;
            display:flex;
            flex-direction: column;
        }
        label {
            margin: 5px;
        }
        .error {
            font-size: 0.7em;
            color: red;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        Firstname<sup>*</sup>: <input type="text" name="firstname" value="<?php echo valueCheck('firstname'); ?>" placeholder="John">
        <?php
        /*if(isset($hiba['firstname'])){
            echo $hiba['firstname'];
        }*/
        echo errors('firstname');
        ?>
    </label>
    <label>
        Lastname<sup>*</sup>: <input type="text" name="lastname" value="<?php echo valueCheck('lastname'); ?>" placeholder="Doe">
        <?php echo errors('lastname'); ?>
    </label>
    <label>
        Employee number: <input type="text" name="employeenumber" value="<?php echo valueCheck('employeenumber', $employeenumber_helper); ?>" placeholder="4000">
        <?php echo errors('employeenumber');?>
    </label>
    <label>
        Email<sup>*</sup>: <input type="text" name="email" value="<?php echo valueCheck('email'); ?>" placeholder="ab@email.cim"> <?php echo errors('email'); ?>
    </label>
    <label>
        Extension: <input type="text" name="extension" value="" placeholder="x4007">
    </label>
    <label>
        Jobtitle: <input type="text" name="jobtitle" value="" placeholder="Mr. President">
    </label>

    <button>New employee</button>
</form>

</body>
</html>
<?php

//eljárás a valuek visszaírásához
function valueCheck($fieldname, $rowdata = ''){
    //ha van az adott elem a postban, mindenképp azt adjuk vissza, ha nincs, akkor a lekért adatot adjuk vissza, ha az sincs akkor semmit
    $elem = filter_input(INPUT_POST, $fieldname);
    if($elem !== null) return $elem;//volt post adat
    //type ellenőrzés javítása ez alapján: '' és 0 is jó kell legyen ... | https://www.php.net/manual/en/types.comparisons.php
    //ha kaptunk rowdatatát visszatérünk azzal, ami ha nem jött paraméterként, akkor üres string
    return $rowdata;
}

//eljárás a hibák kiírásához
/**
 * nem biztos hogy létezik a $hiba tömb, mert csak nem üres postnál hozzuk létre
 * A folyamatot le kell írni, hogy a hiba szerkezete fieldname => errormessage - (~fejlesztői doksi)
 */
function errors($fieldname){
    global $hiba;//az eljárás látja a változót
    //var_dump($hiba);
    if(isset($hiba[$fieldname])) return $hiba[$fieldname];
}