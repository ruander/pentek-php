<?php
require "../7/connect.php";//db csatlakozás
$employeenumber = filter_input(INPUT_GET,'update',FILTER_VALIDATE_INT);
//adatai a db-ből
$qry = "SELECT * FROM employees WHERE employeenumber = $employeenumber LIMIT 1";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$employee = mysqli_fetch_assoc($result);
var_dump($employee);

/**
 * @todo: hf a listából kapott employeenumber adatokat felhasználva ki kell tölteni az űrlapot, az adatokat hibakezelni és ha sikeres a módosítás akkor visszatérni a listázáshoz
 * ellenőrizni hogy egyáltalán sikerült e betölteni adatokat, ha nem, hibaüzi, vagy vissza a listához
 *ha igen, kell egy az adatokkal kitöltött űrlap
 * ne tudjak rámódosítani másik létező employeenumberre
 * ha nem kötelező adatmezőt ürítek (pl extension) akkor az a mező ürüljön a db ben -> vissza a listára
 * minden jó modosítás után -> vissza a listára
 * minden hibás mező mellet hibaüzenet
 * Amennyiben vizsgának szeretnéd beszámoltatni, lehessen létező irodákból választani felvitelkor (most beégetett 3as iroda van az űrlapon) -> ezt elég új felvitelnél megoldani
 */








//eljárás a valuek visszaírásához
function valueCheck($fieldname, $rowdata = ''){
    //ha van az adott elem a postban, mindenképp azt adjuk vissza, ha nincs, akkor a lekért adatot adjuk vissza, ha az sincs akkor semmit
    $elem = filter_input(INPUT_POST, $fieldname);
    if($elem !== null) return $elem;//volt post adat
    //type ellenőrzés javítása ez alapján: '' és 0 is jó kell legyen ... | https://www.php.net/manual/en/types.comparisons.php
    //ha kaptunk rowdatatát visszatérünk azzal, ami ha nem jött paraméterként, akkor üres string
    return $rowdata;
}

//eljárás a hibák kiírásához
/**
 * nem biztos hogy létezik a $hiba tömb, mert csak nem üres postnál hozzuk létre
 * A folyamatot le kell írni, hogy a hiba szerkezete fieldname => errormessage - (~fejlesztői doksi)
 */
function errors($fieldname){
    global $hiba;//az eljárás látja a változót
    //var_dump($hiba);
    if(isset($hiba[$fieldname])) return $hiba[$fieldname];
}