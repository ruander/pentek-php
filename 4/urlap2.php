<?php
if(!empty($_POST)){//ha van adat a postban
    var_dump($_POST);//POST szuperglobális tömb tartalma
    //echo $_POST["nev"];//Ezt így szuperglobális tömbből soha!!!!!!!! mindig filterezz!!!!!!
    //adat tisztázása

    $hiba = [];//üres hibatömb létrehozása - ebbe tesszük majd ha van hiba a mezővel azonos kulcsra
    $nev = filter_input(INPUT_POST,"nev");//default filter - INPUT_POST egy rendszerállandó - Konstans
    //kötelező adatmező ellenőrzése - ha a nev változó üres string akkor hibaüzenetet kéne generálni
    if($nev == ""){//ha nem kaptunk nevet azaz üres maradt
        $hiba['nev']='<span class="error">Kötelező kitölteni!</span>';//hiba eltárolása hogy a formba ki tutdjuk írni
    }

    //email ellenőrzése
    $email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);//@todo https://www.php.net/manual/en/filter.filters.validate.php
    if(!$email){
        $hiba['email'] = '<span class="error">Nem megfelelő formátum!</span>';
    }

    //jelszó ellenőrzése -- min 6 karakter
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');

    if(mb_strlen($pass,"utf-8") < 6 ){//jelszó hossz min 6
        $hiba['pass'] = '<span class="error">Jelszó túl rövid!</span>';
    }elseif($pass != $repass){//ha a hossz jó, de nem egyeztek
        $hiba['repass'] = '<span class="error">Jelszavak nem egyeztek!</span>';
    }else{//jelszó jó
        //elkódolás:
        /*
        //$pass = md5($pass);//md5 NEM biztonságos ebben a formába!!!!
        for($i=1;$i<10;$i++){//így biztonságosabb - de inkább mellőzd
            $pass = md5($pass);
        }
        */
        $pass = password_hash($pass,PASSWORD_BCRYPT);//biztonságos!
        var_dump($pass);
    }

    //hibekezelés kész
    if(empty($hiba)){
        //minden adat jó
        //die('ok');//die megállitja a kódfutást
        //most tároljuk el egy fileban, ha létezik, írjuk felül - registration/YYYY-MM-DD-HHMMSS.user
        /*
        1. mappa ellenőrzés/létrehozás
        2. filenév kialakítása
        3. Tárolni kívánt adatstruktúra szöveggé alakítása (string)
        Név: xy
        Email: a@b.c
        Jelszó (hash): krixkrax
        4. file létrehozása
         */
        $dir = "registration/";//ide akarjuk létrehozni a filet
        if(!is_dir($dir)){//ha nincs
            mkdir($dir,0755, true);//létrehozzuk - @todo hf: mappa jogosultságot áttekintése https://www.php.net/manual/en/ref.dir.php
        }

        $filename = date("Y-m-d-His").'.user';
        //die($dir.$filename);

        $fileContent =
"Név: $nev
Email: $email
Jelszó (hash): $pass";

        file_put_contents($dir.$filename,$fileContent);//@todo hf: fopen,fread,fwrite,fclose https://www.php.net/manual/en/ref.filesystem.php

        //olvassuk vissza az adatokat a fileból és próbáljuk egy tömbben visszaadni ["xy","a@b.c","krixkrax"]
        $tartalom = file_get_contents($dir.$filename);
        //php eol -> rendszer sorzárás
        $adattomb = explode(PHP_EOL,$tartalom);//tömmbé alakítás a sortöréseknél
        var_dump('<pre>');
/*array(3) {
  [0]=>
  string(16) "Név: afddsgdfsg"
  [1]=>
  string(23) "Email: hgy@iworkshop.hu"
  [2]=>
  string(76) "Jelszó (hash): $2y$10$fiF79iJI5uLX6krgg24zK.ix0VojHGpPhOYpexC3k0S/Hieeu5KQS"
}*/
$user = [];//ide jönnek az adatok
//járjuk be a tömböt és minden értéket bontsunk meg a : nál és a kapott tömb [1] ról megkapjuk az nyers értékeket (spacel)
        foreach($adattomb as $v){
            $adat = explode(':',$v)[1];//->
            /*
             * a bemutatot struktúrában az adatokat az adat nevétől mindig egy [:] választja le, ezért a kapott tömb mindig [1] es kulcsa tartalmazni fogja a keresett értéket, hosszabban:
             $adatok = explode(':',$v);
            $adat = $adatok[1];
             */
            $user[] = trim($adat);//eltároljuk a kivánt tömbbe, és a kezdő-záró spaceket letakarítjuk
            //az egész művelet kompaktabban
            //array_push($user,explode(':',$v)[1]);
        }
        var_dump($user);
    }

}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás ugyanabban a fileban</title>
    <style>
        label {
            margin:5px 0;
            display:block;
        }
    </style>
</head>
<body>
<form method="post">
    <label>Név<sup>*</sup>
        <input type="text" name="nev" placeholder="John Doe" value="<?php echo filter_input(INPUT_POST,'nev');//alapszűrés ?>">
        <?php
        //ha van hiba, írjuk ki
        if(isset($hiba['nev'])){//ha létezik a hiba tömb nev kulcsa - ha a tömb se létezik is falset ad
            echo $hiba['nev'];//hiba kiírása
        }
        ?>
    </label>
    <label>Email<sup>*</sup>
        <input type="email" name="email" placeholder="email@cim.hu" value="<?php echo filter_input(INPUT_POST,'email');//alapszűrés ?>">
        <?php
        //ha van hiba, írjuk ki
        if(isset($hiba['email'])){//ha létezik a hiba tömb email kulcsa - ha a tömb se létezik is falset ad
            echo $hiba['email'];//hiba kiírása
        }
        ?>
    </label>
    <label>Jelszó<sup>*</sup> (min. 6 karakter)
        <input type="password" name="pass" value="">
        <?php
        //ha van hiba, írjuk ki
        if(isset($hiba['pass'])){//ha létezik a hiba tömb pass kulcsa
            echo $hiba['pass'];//hiba kiírása
        }
        ?>
    </label>
    <label>Jelszó mégegyszer<sup>*</sup>
        <input type="password" name="repass" value="">
        <?php
        //ha van hiba, írjuk ki
        if(isset($hiba['repass'])){//ha létezik a hiba tömb pass kulcsa
            echo $hiba['repass'];//hiba kiírása
        }
        ?>
    </label>
    <button>Elküldés</button>
</form>
</body>
</html>