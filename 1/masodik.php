<?php
//pure PHP file
/*
PHP ->  PHP Hyper-Text Preprocessor
*/
//Változók 1 - Primitívek
$integer = 2323;// operátor: $ -> változo ; = -> értékadó operátor
$szoveg = '12.04 egy decemberi nap volt, ekkor 5 ember jött el...'; 
$lebego = 7/6;//floating point number
$logikai = true;//boolean vagy bool (true|false)

//változó 'kiírása' -> kiírás közben minden stringé konvertálódik
echo $szoveg;
echo '<br>';//sortörés
echo $lebego;

//változó elnevezési konvenciók
$sudzfcz233mkhn = 35; //helyes de ne használjuk így !!!! nem beszédes
$owned_cars = 35;//beszédes !! jó!! snake case
$ownedCars = 35;//beszédes! jó! camel case
//$1owned_car = 35; //syntax error!!!!!!
$VALAMI = 35; //helyes, de nem használunk általába uppercase változót, csak különleges esetben

//változó tipusa és értéke kiírása - CSAK FEJLESZTÉS ALATT!!!
echo '<pre>';
var_dump($szoveg,$integer,$lebego,$logikai);
echo '</pre>';

//műveletek változókkal
//alapműveletek végezhetőek: +, - , * , /
//művelet idejére automatikus tipus konverzió van
$eredmeny = $integer + $lebego;
echo $eredmeny;
$eredmeny = $szoveg * 2;//felülírjuk az eredmény értékét; ebben az esetben a szöveget konvertálja számmá erről warning vagy notice keletkezik
echo '<br>'.$eredmeny;//operátor: . -> konkatenátor
$szoveg = 'teszt';
echo '<br>';
echo $eredmeny = 50/$lebego;//értékadás és kiírás egy lépésben

//véletlen szám generálása
//'dobj egy kockával' és írd ki a dobás eredményét
$kiiras = '<h1>A generált szám:';
$dobas1 = rand(1,6);//változóban tároljuk a 'dobást'
$kiiras = $kiiras . $dobas1 ;
$kiiras .= '</h1>';//$kiiras = $kiiras .'</h1>'
echo $kiiras;

//dobjunk mégegyszer és a 2 dobás értékének összegét írjuk ki
$dobas2 = rand(1,6);

echo '<br>a $dobas1 és $dobas2 összege: $dobas1+$dobas2';
echo "<br>a $dobas1 és $dobas2 összege: $dobas1+$dobas2";
//ugyanazok a másik operátorral
echo "<br>a \$dobas1 és \$dobas2 összege: \$dobas1+\$dobas2";// \ operátor : escape
echo '<br>a '.$dobas1.' és '.$dobas2.' összege: '.$dobas1.'+'.$dobas2;//string és változó fűzése
echo '<h2>Ja és az összeg:'.($dobas1+$dobas2).'</h2>';