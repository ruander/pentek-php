<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2019. 12. 13.
 * Time: 9:15
 */
//dobj egy kockával 5x és az összeget írd ki
//$dobas = rand(1,6);//egy dobás
/*
for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálata;ciklusváltozó léptetése){
        ciklusmag
}
*/
//összeg
$sum = 0;
for($i=1;$i<=5;$i=$i+1){
    //ciklusmag
    $sum = $sum + rand(1,6);//aktuális dobás hozzáadása az összeg addigi értékéhez
}
echo '<br>A dobások összege:'.$sum;
//ugyanez operátorok használatával
// $i=$i+1 -> $i++;
// $sum = $sum + rand(1,6) -> $sum += rand(1,6)
$sum = 0;//összeg kinullázása
for($i='1';$i<='5';$i++){
    //ciklusmag
    $sum += rand(1,6);//aktuális dobás hozzáadása az összeg addigi értékéhez
}
echo '<br>A dobások összege:'.$sum;
echo '<br>';
$a=5;
echo $a++;//5
echo '<br>';
echo ++$a;//7

//dobjunk egy kockával és mondjuk meg mi volt a dobás és hogy az páros vagy páratlan
$dobas = rand(1,6);
$maradek = $dobas%2;//maradékos osztás ez 2 nél egész számok esetében 0 (páros) vagy 1 (páratlan)
/*
elágazás
if(condition){
    true ág
}else{
    false ág
}
 */
if($maradek == 0){//érték egyenlőség vizsgálat ==
    //igaz ág (páros)
    echo "<br>A dobás értéke: $dobas, ami páros";
}else{
    //hamis ág
    echo "<br>A dobás értéke: $dobas, ami páratlan";
}
//refaktor 1
$dobas = rand(1,6);
$maradek = $dobas%2;
$output = "<br>A dobás értéke: $dobas, ami ";//kiírandó elemek ide kerülnek egymás után FŰZVE!!!!
if($maradek === 0){//érték és típus egyenlőség vizsgálat ===
    //igaz ág (páros)
   $output .= 'páros';
}else{
    //hamis ág
   $output .= 'páratlan';//direkt hiba
}
echo $output;
//refaktor 2 inverse if
$dobas = rand(1,6);
$maradek = $dobas%2;
$output = "<br>A dobás értéke: $dobas, ami ";//kiírandó elemek ide kerülnek egymás után FŰZVE!!!!
if($maradek !== 0){//érték és típus egyenlőtlenség vizsgálat ===
    //igaz ág (páros)
    $output .= 'páratlan';
}else{
    //hamis ág
    $output .= 'páros';
}
echo $output;

//1-10 a számok összege
$sum = 0;
for($i=1;$i<=10;$i++){
    $sum += $i;
}
echo '<br>az összegük:'.$sum;

$tomb=[3,54,2,65,77];
//tömb bejárása ciklussal 1
for($i=0; $i < count($tomb) ;$i++){
    //a ciklusváltozó lehet a tömb kulcsa
    echo "<br>Aktuális kulcs: $i - érték a kulcson: ".$tomb[$i];
}
//asszociatív (és egyébként is...) tömb bejárása
$user = [
    'id' => 5,
    'email' => 'hgy@ruander.hu',
    'name' => 'Horváth György',
];

//foreach
/*
foreach(tömb as $k => $v ){
    ciklusmag ahol eléred az aktuális kulcsot ($k) és értéket ($v)
}
*/
foreach($user as $k => $v){
    echo "<br>Aktuális kulcs: $k - érték a kulcson: $v";
}

$dobasok = [];//üres tömb a dobásoknak
//dobjunk 5x ,irjuk ki az összegüket, majd a dobások értékeit
for($i=1;$i<=5;$i++){
    $dobasok[$i]=rand(1,6);//dobás eltárolása tömbbe
}

echo "<br>A dobások összege:".array_sum($dobasok);

//dobások kiírása a tömb bejárásával
foreach($dobasok as $k => $v){
    echo "<br>$k. dobás: $v";
}

//a while ciklus
/*
 while(condition){
    ciklusmag
}
 */
//minta, ha 'for' szerűen használjuk
$dobasok = [];//tömb ürítése
$i=1;//'ciklusváltozó' alaphelyzet
while($i<=5){
    $dobasok[$i]=rand(1,6);
    $i++;//gondoskodnunk kell, hogy a belépési feltétel megszűnjön, ciklusváltozó léptetése
}

echo "<br>A dobások összege:".array_sum($dobasok);

//dobások kiírása a tömb bejárásával
foreach($dobasok as $k => $v){
    echo "<br>$k. dobás: $v";
}
//while -al így 'ésszerűbb'
$dobasok = [];
while(count($dobasok)<5){//a tömb elemszáma a 'ciklusváltozó'
    $dobasok[]=rand(1,6);//0-as kulcstól elkezdi feltölteni a tömbböt
}
echo "<br>A dobások összege:".array_sum($dobasok);

//dobások kiírása a tömb bejárásával
foreach($dobasok as $k => $v){
    echo "<br>" . ++$k . ". dobás: $v";//sorszám 'rendbehozása'
}
/**
$dobasok = [];
while(count($dobasok)<5){//a tömb elemszáma a 'ciklusváltozó'
    $dobasok[]=rand(1,6);//0-as kulcstól elkezdi feltölteni a tömbböt
}
 *
 * //működik de nem túl szép: ciklusváltozó mentes for
$dobasok = [];
for(;count($dobasok)<5;){
    $dobasok[]=rand(1,6);
}
var_dump($dobasok);*/

//lotto
$sorsolas = [];
while(count($sorsolas)<5){
    $sorsolas[]=rand(1,90);
}

echo '<pre>'.var_export($sorsolas,true).'</pre>';
//egyenlőség nem megengedett
$egyedi = array_unique($sorsolas);
//echo '<pre>'.var_export($egyedi,true).'</pre>';
//lotto ismétlődések nélkül
$sorsolas = [];
while(count($sorsolas)<7){
    $sorsolas[]=rand(1,35);
    $sorsolas = array_unique($sorsolas);
}
echo '<pre>'.var_export($sorsolas,true).'</pre>';