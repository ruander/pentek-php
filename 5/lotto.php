<?php
//folyamatábra alapján

//erőforrások
$huzasok_szama = 5;
$limit = 90;
$hiba = [];
$eredeti_tippek = [];
/***********************************/
if (!empty($_POST)) {

    //hibakezelés
    //var_dump( $_POST);
    //szűrő beállítása
    $args = [
        'tippek' => [
            'filter' => FILTER_VALIDATE_INT,//minden eleme egész szám kell legyen
            'flags' => FILTER_REQUIRE_ARRAY, //maga a tippek tömb kell legyen
            'options' => [
                'min_range' => 1, //minimum tippérték lehet
                'max_range' => $limit //maximum tippérték lehet
            ]
        ],
        'email' => FILTER_VALIDATE_EMAIL

    ];
    $filtered_post = filter_input_array(INPUT_POST, $args);//POST szűrése a beállított paraméterek alapján 1 lépésben
    //var_dump($filtered_post);
    //email
    if(!$filtered_post['email']){
       $hiba['email'] = '<span class="error">hibás formátum!</span>';
    }
    //tippek hibakezelése a szűrt elemekből
    $tippek = $filtered_post['tippek'];
    //eredeti tippek későbbi műveletekhez 'szűrés nélkül'
    $eredeti_tippek = filter_input_array(INPUT_POST, ['tippek' => ['flags' => FILTER_REQUIRE_ARRAY]] );
    $eredeti_tippek = $eredeti_tippek['tippek'];
    $egyedi_tippek = array_unique($eredeti_tippek);//az eredeti egyedi értékes változata
    //ismétlődések vizsgálata
    //var_dump('<pre>',$eredeti_tippek);
    //tippek tömb bejárása
    $hibaUzenet = '<span class="error">hibás adat!</span>';
    foreach($tippek as $key => $tipp){
        if(!$tipp){//ha false a szűrés miatt a tipp azaz hibás adat
            $hiba['tippek'][$key]=$hibaUzenet;
        }
        //ismétlődés jelzése
        if(!array_key_exists($key,$egyedi_tippek)){
            //ha létezik már hibája a mezőnek, hozzáfűzzük
            if(isset($hiba['tippek'][$key])) {
                $hiba['tippek'][$key] .= '<span class="error">Már szerepelt az érték!</span>';
            }else{//különben deklaráljuk
                $hiba['tippek'][$key] = '<span class="error">Már szerepelt az érték!</span>';
            }
        }
    }
    //$tipphibak = array_filter($tippek, function($v){ return !$v;});//7.3-ig így
    //$tipphibak = array_filter($tippek, fn($v) => !$v); //ez csak 7.4 es PHP tól működik : arrow function
    //$hiba['tippek']=array_fill_keys(array_keys($tipphibak),'<span class="error">hibás adat!</span>');

    //var_dump($hiba,'</pre>');
    if (empty($hiba)) {//jó adatok
            die('minden OKÉ!');
    }
}
$form = "<h1>$huzasok_szama/$limit Lottójáték</h1>";
$form .= '<form method="post">';//form nyitás
//ciklus a tipp mezőknek
for ($i = 1; $i <= $huzasok_szama; $i++) {

    $form .= '<label><br>Tipp ' . $i . ': 
           <input type="text" name="tippek[' . $i . ']" placeholder="1" value="'.tippCheck($eredeti_tippek,$i).'">';
    //tippmező hiba hozzáfűzése a form elemhez
    if(isset($hiba['tippek'][$i])){//ha létezik az adott elem a hibatömbben
        $form .= $hiba['tippek'][$i];//hozzáfűzzük a hbaüzenetet az adott mezőhöz
    }
    $form .='</label>';
}
//email mező
$form .= '<br><label>email<sup>*</sup> <input type="text" name="email" placeholder="email@cim.hu" value="'.valueCheck('email').'">';
//email mezőhiba formhoz fűzése
if(isset($hiba['email'])){
    $form .= $hiba['email'];
}
$form .='</label>';
$form .= '<br><button>Tippek feladása</button></form>';//küldés gomb és form zárása
echo $form;

//saját eljárások
/**
 * Eljárás érték visszaadáshoz a value attributumba a html input elemekhez
 * @param $name
 * @return mixed
 */
function valueCheck($name){
    //post adatok globálisak
    //ha van a poston az adott néven érték visszadjuk azt, ha nincs, semmit
    $elem = filter_input(INPUT_POST,$name);
    if($elem){
        return $elem;
    }
    return;
}

/**
 * Eljárás a tippek visszaírására a value attributumba a html input elemekhez
 * @param $tippek
 * @param $tippKey
 * @return mixed
 */
function tippCheck($tippek,$tippKey){//átadjuk az eredeti tippeket és az indexet aminek az adata kell, ha az létezik, visszaadjuk
    if(isset($tippek[$tippKey])) return $tippek[$tippKey];//csonka IF , ha csak 1 műveletet csinálsz, a {} elhagyható
    return;
}