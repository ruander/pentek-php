<pre><?php
//csatlakozás adatbázishoz
$link = /*@*/mysqli_connect("localhost","root","","classicmodels") or die('GEBASZ!');// az function hivás előtti @ elnyeli a function üzeneteit
//var_dump($link);


//sql utasítások használata egyszerű, csak stringként kell kezelni őket
$qry = "SELECT CONCAT(firstname,' ',lastname) as Név FROM employees LIMIT 5";
//futtasuk le a lekérést a felépített linken
$result  = mysqli_query($link,$qry) or die(mysqli_error($link));
var_dump($result);
//kapott eredmény lehet üres halmaz 1 vagy több sor
$row = mysqli_fetch_row($result);
var_dump($row);
$row = mysqli_fetch_row($result);
var_dump($row);
//összes eredmény kibontása tömbbe ciklus segítségével
while(null !== $row = mysqli_fetch_row($result) ){
    var_dump($row);
}
//pl:irodák listája
$qry = "SELECT * FROM offices";
//lekérés
$result  = mysqli_query($link,$qry) or die(mysqli_error($link));
//kibontás fajták
$row = mysqli_fetch_assoc($result);//asszociativ tömb az eredmény
var_dump($row);

$row = mysqli_fetch_array($result);//row merge assoc
var_dump($row);

$row = mysqli_fetch_object($result);//objektum
var_dump($row);
//minden kibontása egybe
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
var_dump($rows);