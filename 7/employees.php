<?php
//csatlakozás az adatbázishoz
include_once("connect.php");//mintha ide lenne gépelve /ha nincs file, warning
require_once("connect.php");//mintha ide lenne gépelve /megáll ha nincs és nem fut tovább
// _once bővítéssel csak akkor töltődik be ha még nem volt betöltve
//var_dump($link);
$qry = "SELECT * FROM employees";//lekérés összeállítása
$result = mysqli_query($link,$qry) or die( mysqli_error($link) );//lekérés
//kibontás ciklusban, asszociatív tömbbe
$nr = 0;
$output = '<ul>';//lista nyitás
while(null !== $row = mysqli_fetch_assoc($result)){
    $nr++;//ez a sorszám
    //listaelemek
    $output .= '<li>'.$nr.': '.implode(' | ',$row).'</li>';

    //var_dump($row);
}
$output .= '</ul>';//lista zárás


//irodák táblázatba gyűjtése
$qry = "SELECT officeCode,country,state,city FROM offices";
$result = mysqli_query($link,$qry) or die( mysqli_error($link) );//lekérés
$output .= '<table border="1">
                <tr>
                 <th>Kód</th>
                 <th>Ország</th>
                 <th>Állam</th>
                 <th>Város</th>
                </tr>';//táblázat nyitás (itt az employee list az outputban van!!!!
while(null !== $row = mysqli_fetch_row($result)){

    $output .= '<tr>';//sorok nyitásai
        //cellák
        $output .= '<td><a href="office.php?code='.$row[0].'">'.$row[0].'</a></td>';
        $output .= '<td>'.$row[1].'</td>';
        $output .= '<td>'.$row[2].'</td>';
        $output .= '<td>'.$row[3].'</td>';
    $output .= '</tr>';//sorok zárásai
    //var_dump($row);
}
$output .= '</table>';
echo $output;