<?php
//változók 2
/**
 * tömbök (array) - vagy halmazok
 */
$tomb = array(); //üres tömb létrehozása (old school)
echo '<pre>';//mert most csak dumpolunk, az meg legyen olvashatóan formázva
var_dump($tomb);//type: array; size:0
//nem létező változó megszólítása műveletre már hibát(notice) okoz:
var_dump($nemletezo);
$tomb = [150, pi(), 'Hello', true, 'World'];//újradeklarálás vagy override
var_dump($tomb);
$tomb[] = 'Teszt szöveg';//automatikus következő üres kulcs
var_dump($tomb);
$tomb[100] = 'Ez a 100-as kulcs';//irányított kulcsra értékadás
var_dump($tomb);
$tomb[] = 'A teszt ujra';
var_dump($tomb);//101
//tömb nem primitív, echozni csak primitívet lehet
echo $tomb;//nem jo : Array + notice
echo '<br>' . $tomb[2];//a kulcson található érték itt primitiv, azaz kiírható

//asszociatív tömbök
$user = [
    'id' => 12,
    'email' => 'hgy@ruander.hu',
    'status' => 1,
    'newsletter' => 1,
    'order_sum' => 0
];
var_dump($user);
//user email kiírása:
echo $user['email'];

//több dimenziós tömb
$tomb_uj = [
    'email' => 'hgy@ruander.hu',
    'schedule' => [
        'monday' => true,
        'tuesday' => false,
        'wednesday' => true,
        'thursday' => false,
        'friday' => true,
        'saturday' => null,
        'sunday' => null
    ],
];
var_dump($tomb_uj);
//tömb műveletek
echo '<br>A $tomb hossza:'.count($tomb);
echo '<br>A $tomb maximuma:'.max($tomb);
echo '<br>A $tomb minimuma:'.min($tomb);

$kiiras = implode(',' , $tomb );//string a kimenet, ami a , -vel elválasztott értékeket tartalmazza
echo "<br>$kiiras";
//vissza tömbbé a  vesszőknél bontva
$tomb_szovegbol = explode(',',$kiiras);
var_dump($tomb_szovegbol);//uj kulcsokat kap mert uj tömböt építünk csak értékekből
