<?php
$secret_key = 'S3cr3T_k3Y!';//hasheléshez

/*modulok rendszer szintű beállítása*/
$modulDir = 'modules/';//itt vannak a moduljaink
$modulExt = '.php'; //a rendszer modulefileok .php kiterjesztésűek (lehet más is)

//admin menü pontjai mit irjunk ki, hova mutasson, ikon, milyen file hajtja, mp azonositoja
$adminMenu = [
    0 => [
        'title' => 'vezérlőpult',
        'moduleFile' => 'dashboard',
        'icon' => 'fas fa-tachometer-alt' //adminlte3 illetve fontawesome 5 höz előkészítve
    ],
    1 => [
        'title' => 'adminok',
        'moduleFile' => 'admins',
        'icon' => 'fa fa-fw fa-users'
    ],
    2 => [
        'title' => 'cikkek',
        'moduleFile' => 'articles',
        'icon' => 'fas fa-newspaper'
    ],
    4 => [
        'title' => 'Kuponjaim',
        'moduleFile' => 'coupons',
        'icon' => 'fas fa-ticket-alt',
    ],
    5 => [
        'title' => 'sorsolás',
        'moduleFile' => 'lots',
        'icon' => 'fas fa-trophy',
        'privilege' => 'hgy@iworkshop.hu',//ő sorsolhat
    ],
];

