<?php

//eljárás ami visszaadja komplett admin menüt a kialakított tömbből
function adminMenu($menuTomb = []){
    $ret = '<nav class="mt-2"><ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">';//menu html elemek nyitása
    foreach ($menuTomb as $menuId => $menu ){//menüpontok kialakítása a tömbből
        $ret .= '<li class="nav-item"><a class="nav-link" href="?p='.$menuId.'"><i class="'.$menu['icon'].' nav-icon"></i>'.$menu['title'].'</a></li>';
    }
    $ret .= '</ul></nav>';//html zárások
    return $ret;
}

/**
 * dump() die()
 * @param $var
 */
 function dd($var){
     $msg = '<pre>'.var_export($var,true).'</pre>';
     die($msg);
 }
/**
 * eljárás a valuek visszaadására
 * @param $key elem neve a postban
 * @param string $dbData adatbázisból kapott érték
 * @return mixed|string //post adat, db adat vagy semmi
 */
function checkValue($key, $dbData = '')
{
    $postData = filter_input(INPUT_POST, $key);//ha létezik ilyen post elem
    if ($postData !== null) return $postData;// akkor visszaadjuk azt

    return $dbData;//különben visszaadjuk a db adatot ami, ha nincs akkor üres string
}

/**
 * CheckBox default checker
 * @param $key - input name
 * @param string $dbData - data from database (0-1)
 * @return 'checked' | empty string
 */
function checkBox($key, $dbData = '')
{
    if (!empty($_POST)) {
        if (filter_input(INPUT_POST, $key)) return "checked";
        return '';
    }
    if ($dbData) return "checked";
    return '';
}

/**
 * adatbázisból kapott adatok meglétének ellenőrzésére készített eljárás
 * @param $array | ebben kellenek legyenek az adatok
 * @param $key | ezt a kulcsot keressük az adattömbben
 * @return mixed|null
 */
function hasData($array,$key){
    if(array_key_exists($key,$array)){
        return $array[$key];
    }
    return null;
}
//hiba kiírásra
function hibaKiir($key, &$hiba){//hiba változó referenciaátadása
    //ha nem létezik a hiba tömb, (array_key__existnek az kell!) akkor létrehozunk üreset, ha létezik akkor nem változtatjuk
    $hiba = $hiba?:[];
    //ha létezik az adott kulcs akkor visszaadjuk a rajta található értéket
    if(array_key_exists($key,$hiba)){
        return $hiba[$key];
    }
    return;
}
//kupon megjelenítéshez '-' hozzáadása a tokenhez
function hyphenate($str)
{
    return implode("-", str_split($str, 6));
}
/*
 * string kezelő eljárások
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}

//beléptető eljárás
function login()
{
    global $secret_key, $link;//hogy 'lássa' az eljárásunk

    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    //var_dump($email);

    $password = filter_input(INPUT_POST, 'pass');//real escape string ne menjen rajta, mert a jelszó tartalmazhat speciális karaktereket, és úgy is el lesz 'hashelve'

    $passQry = "SELECT id,pass,username FROM admins WHERE email = '$email' LIMIT 1";
    $passResult = mysqli_query($link,$passQry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($passResult);
    /*if($row){
        //a tesztelendő pass a row[1] eleme mert 2. mező a lekérésben (0,1,2,...)
        $testPassword = password_verify($password.$secret_key, $row[1]);
        dd($testPassword);//saját dump die eljárás
    }*/
    if( password_verify($password.$secret_key, $row[1]) ){
        //van ilyen admin
        $user = [
            'id' => $row[0],
            'email' => $email,
            'username'=> $row[2]
        ];
    }else{
        return false;
    }
    //itt vagy van feltöltött user tömb vagy el se jut idáig a kód -> return false ág!

//https://www.geeksforgeeks.org/how-to-get-the-mac-and-ip-address-of-a-connected-client-in-php/
    // PHP code to get the MAC address of Client
    $MAC = exec('getmac');

    // Storing 'getmac' value in $MAC
    $MAC = strtok($MAC, ' ');

    // Updating $MAC value using strtok function,
    // strtok is used to split the string into tokens
    // split character of strtok is defined as a space
    // because getmac returns transport name after
    // MAC address
    //echo "MAC address of client is: $MAC";
    $_SESSION['user'] = $user;//felh adatai
    $_SESSION['id'] = session_id();
    //var_dump('<pre>', $_SERVER);
    $spass = md5($user['id'] . $_SESSION['id'] . $secret_key . $MAC);//MAC addressel a kliens azonosítás fizikailag is megtörténik, bár 'átverhető'
    //die();
    $now = time();//timestamp
    //ha lenne 'beragadt' mf az azonosítóra, töröljük és töröljük a lejárt mf -ot is, öntisztító tábla
    mysqli_query($link, "DELETE FROM sessions WHERE sid ='" . $_SESSION['id'] . "' LIMIT 1") or die(mysqli_error($link));
    //mf adatbázisba mentése
    $qry = "INSERT INTO sessions(sid,spass,stime)
                VALUES('" . $_SESSION['id'] . "','$spass',$now)";
    mysqli_query($link, $qry) or die(mysqli_error($link));

    return true;
}

//auth ellenőrzése
function auth()
{
    global $link, $secret_key;
    $sid = session_id();

    $MAC = exec('getmac');
    $MAC = strtok($MAC, ' ');
    $spass = '';
    if (isset($_SESSION['user']['id'])) {
        $spass = md5($_SESSION['user']['id'] . $sid . $secret_key . $MAC);//ua mint loginnál ,az adatok több helyről összeszedve
    }
    $now = time();//timestamp
//ha lenne 'beragadt' mf az azonosítóra, töröljük és töröljük a lejárt mf -ot is, öntisztító tábla
    $expired = $now - 900; //most 15p
    mysqli_query($link, "DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
//lejárt mf-ok törlése, öntisztítás
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
//ellenőrzés
    if ($spass === $row[0] and $row !== null) {//ha üres a row akkor semmikép sem lehet jó a béléptetés
        //updateljük a stimeot mert user akció volt
        mysqli_query($link,"UPDATE sessions SET stime = $now ") or die(mysqli_error($link));
        return true;
    }
    return false;
}