<?php
require "../config/connect.php";//db csatlakozás
require "../config/env.php";//környezeti változók beállítása (titkosító segéd, stb...)
require "../config/functions.php";//saját eljárások
//erőforrások
$page = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:0;//vagy kapunk egy menupont azonosítót vagy azt mondjuk: 0 (dashboard)
$baseUrl = '?p='.$page;//ez a modulazonosítóval ellátott url

//BETÖLTENDŐ MODUL kialakítása
$moduleFile = $modulDir.$adminMenu[$page]['moduleFile'].$modulExt;//betöltendő rendszermodul fileja

//admin fő file
session_start();//mf indítása
$sid = session_id();

//die(session_id());//visszaadja a kérést indító kliens aktuális munkafolyamat azonosítóját
if (filter_input(INPUT_GET, 'logout')) {//kiléptetünk ha kell
    session_destroy();//roncsoljuk a mf ot.
    unset($_SESSION);//kivesszük a auth reprezentációját
}

$auth = auth();//eljárás true|false
//var_dump($_SESSION);
//die();
if ($auth === false) {
    header('location:login.php');
    exit();
}

//ha létezik a modul töltsük be
    $output = '';
if(file_exists($moduleFile)){//modulfile betöltése még minden html kiírás előtt,  modulfile kialakított $output elembe gyűjti az összes kiirandó dolgát amit a bodyba egy echoval megoldunk
    include $moduleFile;
}
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ruander Oktatóközpont PHP tanfolyam | dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="css/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <?php
    //ha van modulstílus, tegyük ide
    if(isset($moduleStyles)) echo $moduleStyles;
    ?>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
    <?php include "includes/top-navigation.php"; ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="?" class="brand-link">
      <img src="https://picsum.photos/80?z"
           alt="Admin Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Ruander</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://picsum.photos/160?a" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['user']['username']; ?></a>
        </div>
          <div class="logout">
              <a href="?logout=true" class="btn btn-sm btn-danger">kilépés</a>
          </div>
      </div>

      <!-- Sidebar Menu -->
      <?php include "includes/side-navigation.php"; ?>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $adminMenu[$page]['title'];  ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><?php echo $adminMenu[$page]['title'];  ?></h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <?php echo $output; ?>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.3-pre
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>

</body>
</html>
<!-- @todo: belépés után kell egy bejegyzések menüpont ahol szerkeszthető bejegyzések vannak (cím, id, tartalom, szerző, status, készítés dátuma, publikálás dátuma - db tábla terv és crud - admins alapján lehet) -->
