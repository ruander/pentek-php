<?php
//a switch
$szam = rand(1,5);
echo "<h1>$szam</h1>";
//a switch egy olyan elágazás aminek több ága lehet és bemehet több ágba is egyszerre

switch ($szam){
    case 1:
        echo 'egy';
        break;//itt távozik a switchből ha nincs, megy a következő case-re
    case 2:
        echo 'kettő';
        break;
    case 3:
        echo 'három';
        break;
    case 4:
        echo 'négy';
        break;
    case 5:
        echo 'öt';
        break;
    default: echo 'alap';
}