<?php
require "../config/connect.php";//db csatlakozás
require "../config/env.php";//környezeti változók beállítása (titkosító segéd, stb...)
require "../config/functions.php";//saját eljárások
//var_dump($_SESSION);//itt még nem létezik
session_start();//munkafolyamat indítása
    if(auth()){//ha be van jelentkezve mit keres itt? huzzon az adminba vissza
        header('location:index.php');exit();
    }
//var_dump($_SESSION);
if (!empty($_POST)) {
    $hiba = [];
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    if(login()){//ha sikeresen lefut a login eljárás akkor mehetünk az indexre, egyébként hiba
        header('location:index.php');
        exit();
    }else{
        $hiba['login'] = '<span class="error">Nem megfelelő email/jelszó páros!</span>';
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin - Belépés</title>
</head>
<body>
<form method="post">
    <?php
    //hiba kiírása, ha van
    if (isset($hiba['login'])) echo $hiba['login'];
    ?>
    <label>
        Email: <input type="text" name="email" value="<?php echo filter_input(INPUT_POST,'email') ?>" required>
    </label>
    <label>
        Jelszó: <input type="password" name="pass" value="">
    </label>
    <button>Belépés</button>
</form>
</body>
</html>