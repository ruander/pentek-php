<?php
require "../config/connect.php";//db csatlakozás
require "../config/env.php";//környezeti változók beállítása (titkosító segéd, stb...)
require "../config/functions.php";//saját eljárások
//erőforrások
$page = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:0;//vagy kapunk egy menupont azonosítót vagy azt mondjuk: 0 (dashboard)
$baseUrl = '?p='.$page;//ez a modulazonosítóval ellátott url

//BETÖLTENDŐ MODUL kialakítása
$moduleFile = $modulDir.$adminMenu[$page]['moduleFile'].$modulExt;//betöltendő rendszermodul fileja

//admin fő file
session_start();//mf indítása
$sid = session_id();

//die(session_id());//visszaadja a kérést indító kliens aktuális munkafolyamat azonosítóját
if (filter_input(INPUT_GET, 'logout')) {//kiléptetünk ha kell
    session_destroy();//roncsoljuk a mf ot.
    unset($_SESSION);//kivesszük a auth reprezentációját
}

$auth = auth();//eljárás true|false
//var_dump($_SESSION);
//die();
if ($auth === false) {
    header('location:login.php');
    exit();
}

//ha létezik a modul töltsük be
    $output = '';
if(file_exists($moduleFile)){//modulfile betöltése még minden html kiírás előtt,  modulfile kialakított $output elembe gyűjti az összes kiirandó dolgát amit a bodyba egy echoval megoldunk
    include $moduleFile;
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin felület</title>
    <?php
    //ha van modulstílus, tegyük ide
    if(isset($moduleStyles)) echo $moduleStyles;
    ?>
</head>
<body>




<?php
echo 'Üdvözöllek kedves ' . $_SESSION['user']['username'] . '! Admin Felület (belépve) | <a href="?logout=true">kilépés</a>';

echo adminMenu($adminMenu);

//modultartalom kiírása
echo $output;
?>
</body>
</html>
