<?php
//Adminisztrátorok kezelése
//erőforrások
$action = filter_input(INPUT_GET, 'action') ?: 'read'; //művelet tipusa urlből , ha nincs read, azaz listázás
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?: null;
//var_dump($_POST);
$output = '';//ez lesz a kiírandó

$db_table = 'articles';//ez lesz a db tábla amibe az adatok lesznek
//űrlap adatok feldolgozása/hibakezelés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //cím kötelező
    $title = filter_input(INPUT_POST,'title');
    //elkészítjük a seo címet
    $seo_title = ekezettelenit($title);
    if(!$title){
        $hiba['title']= '<span class="error">kötelező kitölteni!</span>';
    }
    //szerző, ha nincs adjunk
    $author = filter_input(INPUT_POST,'author')?:'Nameless One';
    //status
    $status = filter_input(INPUT_POST, 'status') ? 1 : 0;
    if (empty($hiba)) {
        //adatok tisztázása
        $now = date('Y-m-d H:i:s');//datetime

        $data =[
            'title' => $title,
            'seo_title' => $seo_title,
            'status' => $status,
            'author' => $author,
        ];//ezek az adatok mennek a db-be
        //update/crate szétválasztása
        if ($action == 'create') {
            //uj cikk felvitele a segédtömb alapján a kulcsok és értékek felhasználásával
           $qry = "INSERT INTO `$db_table` (`" . implode("`,`", array_keys($data)) . "`) 
                    VALUES ('" . implode("','", $data) . "')";
        } else {//update

            $recordSet = [];
            foreach ($data as $key => $value) {
                $recordSet[] = "$key = '$value' ";
            }
            //dd($recordSet);
            $qry = "UPDATE $db_table SET ".implode(',',$recordSet)." WHERE id = $tid";
        }
        mysqli_query($link, $qry) or die(mysqli_error($link));//insert vagy error
        //visszairányítunk listázásra
        header('location:' . $baseUrl);//átirányítás a modul betöltésre
        exit();
        //die('jók az adatok!');
    }
}

//switch a működés szétválasztására
switch ($action) {
    case 'delete':
        //törlés
        if ($tid) {
            mysqli_query($link, "DELETE from $db_table WHERE id = $tid") or die(mysqli_error($link));
        }
        //visszairányítunk listázásra
        header('location:' . $baseUrl);
        exit();
        break;//ez nem kellene de maradjon benn (exit miatt sose jut ide ez az ág)
    case 'update':
        //módosítás
        if ($tid) {
            $qry = "SELECT * FROM $db_table WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $rowData = mysqli_fetch_assoc($result);
            //var_dump($user);
        }
    //break;
    case 'create':
        $rowData = isset($rowData)? $rowData:[];//ha nincs db adat legyen üres tömb helyette az ellenőrző eljárásaink miatt léteznie kell
        //új felvitel űrlap
        $form = '<form method="post" class="default-form">
            <fieldset>
                <label>Cím<sup>*</sup>
                    <input type="text" name="title" placeholder="bla bla bla" value="' . checkValue('title', hasData($rowData, 'title')) . '">';//űrlap elem értékének visszaírása
        if (isset($hiba['title'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['title'];
        }
        $form .= '</label>
                <label>Seo Cím (automatikus)
                    <input type="text" name="seo_title"  value="' . (isset($seo_title)?$seo_title:hasData($rowData, 'seo_title')) . '" disabled></label>';//űrlap elem értékének visszaírása - ez a mező csak tájékoztatás, nem innen születik meg az adat hanem mindig a titleből generáljuk
//szerző, default-belépett admin neve
        $form .= '</label>
                <label>Szerző
                    <input type="text" name="author"  value="'
            . checkValue('author',hasData($rowData, 'author')?:$_SESSION['user']['username'])
            . '"></label>';
//aktív - inaktív
        $form .= '<label><input type="checkbox" name="status" value="1" ' . checkBox('status', hasData($rowData, 'status')) . ' > aktív?</label>';
        $form .= '</fieldset>';

//form zárása és a gomb
        $form .= '<button class="btn btn-success" type="submit">' . ($action == 'create' ? 'Új Felvitel' : 'Módosítom') . '</button>
</form>';
        $output .= $form;
        break;
    default:
        //lista
        $qry = "SELECT * FROM $db_table";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<div class="row">
                    <div class="col"><a href="' . $baseUrl . '&amp;action=create" class="btn btn-primary">Új felvitel</a>
                    <table class="table table-striped table-responsive">
                    <tr>
                        <th>id</th>
                        <th>cím</th>
                        <th>szerző</th>
                        <th>státusz</th>
                        <th>művelet</th>
                    </tr>';//table nyitás és fejléc
        //sorok
        while ($row = mysqli_fetch_assoc($result)) {
            $table .= '<tr>
                        <td>' . $row['id'] . '</td>
                        <td>' . $row['title'] . '</td>
                        <td>' . $row['author'] . '</td>
                        <td>' . $row['status'] . '</td>
                        <td><div class="actions"> 
                        <a href="' . $baseUrl . '&amp;action=update&amp;id=' . $row['id'] . '"><i class="far fa-edit"></i></a> <a href="' . $baseUrl . '&amp;action=delete&amp;id=' . $row['id'] . '"><i class="fas fa-trash-alt"></i></a>
                       </div> </td>  
                    </tr>';
        }
        $table .= '</table>
</div></div>';
        $output .= $table;
        break;
}


//kiírás majd az indexben


//styles
$moduleStyles = "<style>
.registration-form fieldset {
display: flex;
flex-flow: column nowrap;
}
label {
    display:block;
}
.error {
    font-style: italic;
    color:red;
}
</style>";




