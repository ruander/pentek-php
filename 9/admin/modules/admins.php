<?php
//Adminisztrátorok kezelése
//erőforrások
$action = filter_input(INPUT_GET, 'action') ?: 'read'; //művelet tipusa urlből , ha nincs read, azaz listázás
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?: null;
//var_dump($_POST);
$output = '';//ez lesz a kiírandó

$db_table = 'admins';//ez lesz a db tábla amibe az adatok lesznek
//űrlap adatok feldolgozása/hibakezelés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //name,email,tippek
    $username = trim(filter_input(INPUT_POST, 'username'));//mező értéken alap szűrése és a spacek eltávolítása
    //név minimum 3 karakter
    if (mb_strlen($username, "utf-8") < 3) {
        $hiba['username'] = '<span class="error">A név minimum 3 karakter kell legyen</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {

        $qry = "SELECT id FROM $db_table WHERE `email` = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //null ha nincs még ilyen, és tömb, ha már van, és ha tömb, akkor a 0 kulcsán van az ID akihez tartozik
        //echo '<pre>'.var_export($row,true).'</pre>';
        if (is_array($row) && $row[0] != $tid) {
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }
    //password
    $password = filter_input(INPUT_POST, 'pass');
    //jelszó ellenőrzése uj esetben mindig kell, módosításkor csak akkor ha 1 karakter legalább van az 1es jelszó mezőbe
    if ($action == 'create' || ($action == 'update' && $password != '')) {
        //min 6 karakter
        if (mb_strlen($password, 'utf-8') < 6) {
            $hiba['pass'] = '<span class="error">min 6 karakter!</span>';
        } else {
            //pass1 rendben, nézzük repass ugyanaz e
            $repass = filter_input(INPUT_POST, 'repass');
            if ($password !== $repass) {
                $hiba['repass'] = '<span class="error">jelszavak nem egyeztek!</span>';
            } else {
                //jelszó oké @todo: loginnál cserélni a kódolást!
                $password = password_hash($password . $secret_key, PASSWORD_BCRYPT);
            }

        }
    }

    //status
    $status = filter_input(INPUT_POST, 'status') ? 1 : 0;
    if (empty($hiba)) {
        //adatok tisztázása
        $now = date('Y-m-d H:i:s');//datetime
        $privileges = 1;//@todo: később ez állítható kell legyen, most nem csinálunk többszintű jogosultságot
        $user = [
            'username' => $username,
            'email' => $email,
            'status' => $status,
            'privileges' => $privileges,
            'time_created' => $now
        ];
        //password csak akkor kell ha nem üres
        if ($password) {
            $user['pass'] = $password;
        }
        //update/crate szétválasztása
        if ($action == 'create') {
            //uj felh. felvitele a segéd $user tömb alapján a kulcsok és értékek felhasználásával
            $qry = "INSERT INTO `$db_table` (`" . implode("`,`", array_keys($user)) . "`) 
                    VALUES ('" . implode("','", $user) . "')";
        } else {//update
            $user['time_updated'] = $now;//most módosítjuk
            $uSet = [];
            foreach ($user as $key => $value) {
                $uSet[] = "$key = '$value' ";
            }
            //dd($uSet);
            $qry = "UPDATE $db_table SET ".implode(',',$uSet)." WHERE id = $tid";
        }
        mysqli_query($link, $qry) or die(mysqli_error($link));//insert vagy error
        //visszairányítunk listázásra
        header('location:' . $baseUrl);//átirányítás a modul betöltésre
        exit();
        //die('jók az adatok!');
    }
}

//switch a működés szétválasztására
switch ($action) {
    case 'delete':
        //törlés
        if ($tid && $tid != $_SESSION['user']['id']) {
            mysqli_query($link, "DELETE from $db_table WHERE id = $tid") or die(mysqli_error($link));
        }
        //visszairányítunk listázásra
        header('location:' . $baseUrl);
        exit();
        break;//ez nem kellene de maradjon benn (exit miatt sose jut ide ez az ág)
    case 'update':
        //módosítás
        if ($tid) {
            $qry = "SELECT * FROM $db_table WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $user = mysqli_fetch_assoc($result);
            //var_dump($user);
        }
    //break;@todo: az itt megkapott adatokat valahogy be kéne tenni az űrlapba ami a create ágban van, tehát a create ágnak 'tudnia kell', hogy vannak e db-ből adatok vagy a postból, vagy új üres állapot kell....
    case 'create':
        $user = isset($user) ? $user : [];//ha nincsenek user adatok uj felvitelnél a user tömb legyen üres
        //új felvitel űrlap
        $form = '<form method="post" class="registration-form">
            <fieldset>
                <legend>Felhasználó adatai</legend>
                <label>Email<sup>*</sup>
                    <input type="text" name="email" placeholder="email@cim.hu" value="' . checkValue('email', hasData($user, 'email')) . '">';//űrlap elem értékének visszaírása
        if (isset($hiba['email'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['email'];
        }
        $form .= '</label>
                <label>Név<sup>*</sup>
                    <input type="text" name="username" placeholder="John Doe" value="' . checkValue('username', hasData($user, 'username')) . '">';//űrlap elem értékének visszaírása
        if (isset($hiba['username'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['username'];
        }
//jelszó1
        $form .= '</label>
                <label>Jelszó<sup>*</sup>
                    <input type="password" name="pass"  value="">';
        if (isset($hiba['pass'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['pass'];
        }
        $form .= '</label>';
//jelszó2
        $form .= '<label>Jelszó mégegyszer<sup>*</sup>
                    <input type="password" name="repass"  value="">';
        if (isset($hiba['repass'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['repass'];
        }
        $form .= '</label>';
//aktív - inaktív
        $form .= '<label><input type="checkbox" name="status" value="1" ' . checkBox('status', hasData($user, 'status')) . ' > aktív?</label>';
        $form .= '</fieldset>';

//form zárása és a gomb
        $form .= '<button type="submit">' . ($action == 'create' ? 'Új Felvitel' : 'Módosítom') . '</button>
</form>';
        $output .= $form;
        break;
    default:
        //lista
        $qry = "SELECT * FROM $db_table";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<a href="' . $baseUrl . '&amp;action=create">Új felvitel</a>
                    <table class="table table-striped table-responsive">
                    <tr>
                        <th>id</th>
                        <th>név</th>
                        <th>email</th>
                        <th>státusz</th>
                        <th>művelet</th>
                    </tr>';//table nyitás és fejléc
        //sorok
        while ($row = mysqli_fetch_assoc($result)) {
            $table .= '<tr>
                        <td>' . $row['id'] . '</td>
                        <td>' . $row['username'] . '</td>
                        <td>' . $row['email'] . '</td>
                        <td>' . $row['status'] . '</td>
                        <td> 
                        <a href="' . $baseUrl . '&amp;action=update&amp;id=' . $row['id'] . '">módosít</a> ';
            if ($row['id'] != $_SESSION['user']['id']) {
                $table .= '| <a href="' . $baseUrl . '&amp;action=delete&amp;id=' . $row['id'] . '">töröl</a>';
            }
            $table .= '</td>  
                    </tr>';
        }
        $table .= '</table>';
        $output .= $table;
        break;
}


//kiírás majd az indexben


//styles
$moduleStyles = "<style>
.registration-form fieldset {
display: flex;
flex-flow: column nowrap;
}
label {
    display:block;
}
.error {
    font-style: italic;
    color:red;
}
</style>";




