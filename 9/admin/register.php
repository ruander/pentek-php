<?php
//Adminisztrátorok kezelése
//erőforrások
require "../config/connect.php";//db csatlakozás
require "../config/env.php";//környezeti változók beállítása (titkosító segéd, stb...)
require "../config/functions.php";//saját eljárások

//var_dump($_POST);
$output = '';//ez lesz a kiírandó

$db_table = 'admins';//ez lesz a db tábla amibe az adatok lesznek
//űrlap adatok feldolgozása/hibakezelés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //name,email,tippek
    $username = trim(filter_input(INPUT_POST, 'username'));//mező értéken alap szűrése és a spacek eltávolítása
    //név minimum 3 karakter
    if (mb_strlen($username, "utf-8") < 3) {
        $hiba['username'] = '<span class="error">A név minimum 3 karakter kell legyen</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    } else {

        $qry = "SELECT id FROM $db_table WHERE `email` = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //null ha nincs még ilyen, és tömb, ha már van, és ha tömb, akkor a 0 kulcsán van az ID akihez tartozik
        //echo '<pre>'.var_export($row,true).'</pre>';
        if (is_array($row)) {
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }
    //password
    $password = filter_input(INPUT_POST, 'pass');
    //min 6 karakter
    if (mb_strlen($password, 'utf-8') < 6) {
        $hiba['pass'] = '<span class="error">min 6 karakter!</span>';
    } else {
        //pass1 rendben, nézzük repass ugyanaz e
        $repass = filter_input(INPUT_POST, 'repass');
        if ($password !== $repass) {
            $hiba['repass'] = '<span class="error">jelszavak nem egyeztek!</span>';
        } else {
            //jelszó oké @todo: loginnál cserélni a kódolást!
            $password = password_hash($password . $secret_key, PASSWORD_BCRYPT);
        }

    }
    //status
    $status = filter_input(INPUT_POST, 'status') ? 1 : 0;
    if (empty($hiba)) {
        //adatok tisztázása
        $now = date('Y-m-d H:i:s');//datetime
        $privileges = 1;//@todo: később ez állítható kell legyen, most nem csinálunk többszintű jogosultságot
        $user = [
            'username' => $username,
            'email' => $email,
            'pass' => $password,
            'status' => $status,
            'privileges' => $privileges,
            'time_created' => $now,
            'time_updated' => null
        ];

        //update/crate szétválasztása

        //uj felh. felvitele a segéd $user tömb alapján a kulcsok és értékek felhasználásával
        $qry = "INSERT INTO `$db_table` (`" . implode("`,`", array_keys($user)) . "`) 
                    VALUES ('" . implode("','", $user) . "')";

        mysqli_query($link, $qry) or die(mysqli_error($link));//insert vagy error
        //visszairányítunk listázásra
        header('location:register.php');
        exit();
        //die('jók az adatok!');
    }
}
$form = '<form method="post" class="registration-form">
            <fieldset>
                <legend>Felhasználó adatai</legend>
                <label>Email<sup>*</sup>
                    <input type="text" name="email" placeholder="email@cim.hu" value="' . checkValue('email') . '">';//űrlap elem értékének visszaírása
if (isset($hiba['email'])) {//hiba 'befűzése' az űrlap elemhez ha van
    $form .= $hiba['email'];
}
$form .= '</label>
                <label>Név<sup>*</sup>
                    <input type="text" name="username" placeholder="John Doe" value="' . checkValue('username') . '">';//űrlap elem értékének visszaírása
if (isset($hiba['username'])) {//hiba 'befűzése' az űrlap elemhez ha van
    $form .= $hiba['username'];
}
//jelszó1
$form .= '</label>
                <label>Jelszó<sup>*</sup>
                    <input type="password" name="pass"  value="">';
if (isset($hiba['pass'])) {//hiba 'befűzése' az űrlap elemhez ha van
    $form .= $hiba['pass'];
}
$form .= '</label>';
//jelszó2
$form .= '<label>Jelszó mégegyszer<sup>*</sup>
                    <input type="password" name="repass"  value="">';
if (isset($hiba['repass'])) {//hiba 'befűzése' az űrlap elemhez ha van
    $form .= $hiba['repass'];
}
$form .= '</label>';
//aktív - inaktív
$form .= '<label><input type="checkbox" name="status" value="1" ' . checkBox('status') . ' > aktív?</label>';
$form .= '</fieldset>';

//form zárása és a gomb
$form .= '<button type="submit">Felvitel</button>
</form>';
$output .= $form;


//kiírás
echo $output;

//styles @todo: ne itt irjuk ki hanem legyen vagy fileban vagy a headben
echo $styles = "
<style>
.registration-form fieldset {
display: flex;
flex-flow: column nowrap;
}
label {
    display:block;
}
.error {
    font-style: italic;
    color:red;
}
</style>";




